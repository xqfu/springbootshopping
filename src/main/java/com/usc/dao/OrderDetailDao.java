package com.usc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.beans.OrderDetail;

public interface OrderDetailDao extends JpaRepository<OrderDetail, Integer>{
	List<OrderDetail> findByOrderId(int orderId);

}
