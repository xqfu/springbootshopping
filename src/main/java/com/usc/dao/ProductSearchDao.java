package com.usc.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;


import com.usc.beans.Product;

public interface ProductSearchDao extends JpaRepository<Product, Integer>{
    @Query(value = "select * from product p where p.name like %?1% or p.description like %?1%", nativeQuery = true)
	List<Product> fullTextSearch(String text);
}
