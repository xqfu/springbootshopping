package com.usc.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.beans.User;

public interface UserSearchDao  extends JpaRepository<User, Integer>{
    @Query(value = "select * from usc_user u where u.username like %?1% or u.email like %?1% or u.address1 like %?1% or u.address2 like %?1% or u.city like %?1% or u.state like %?1%", nativeQuery = true)
	List<User> fullTextSearch(String text);
}
