package com.usc.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

//import com.usc.beans.Order;
import com.usc.beans.OrderShow;

public interface OrderShowDao extends JpaRepository<OrderShow, Integer>{
	List<OrderShow> findByUserId(int userId);
}
