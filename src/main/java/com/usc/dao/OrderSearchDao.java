package com.usc.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.JpaRepository;

import com.usc.beans.Order;

public interface OrderSearchDao extends JpaRepository<Order, Integer>{
    @Query(value = "select * from orders o where o.date_created >= %?1% and o.date_created <= %?2%", nativeQuery = true)
	List<Order> fullTextSearch(String date1, String date2);
}
