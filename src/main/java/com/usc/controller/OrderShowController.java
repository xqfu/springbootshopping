package com.usc.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.usc.beans.OrderShow;
//import com.usc.beans.User;
import com.usc.dao.OrderShowDao;
import com.usc.beans.OrderShow;
import com.usc.beans.User;
import com.usc.dao.OrderShowDao;
import com.usc.dao.UserDao;
import com.usc.service.UserService;

@RestController() // recept API request
@RequestMapping("/ordershow")
public class OrderShowController {
	@Autowired
	OrderShowDao orderShowDao;
	
	@Autowired
	UserDao userDao;
	
	@Autowired
	UserService userService;
	
	@GetMapping
	public List<OrderShow> getOrderShowListByName(String username) {
		User user = userDao.findByUsername(username);
		System.out.println("isUserAdmin(user)="+userService.isUserAdmin(user));	
        if(userService.isUserAdmin(user)) {
            return (List<OrderShow>)orderShowDao.findAll();
        } else {
        	System.out.println("not isUserAdmin");
            return orderShowDao.findByUserId(userDao.findByUsername(username).getId());
        }
	}
}
