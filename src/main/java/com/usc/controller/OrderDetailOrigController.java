package com.usc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.usc.beans.OrderDetailOrig;
import com.usc.dao.OrderDetailOrigDao;
import com.usc.http.Response;
import com.usc.service.OrderDetailOrigService;
import com.usc.service.OrderService;

@RestController() // recept API request
@RequestMapping("/orderdetailadd")
public class OrderDetailOrigController {
	@Autowired
	OrderDetailOrigDao orderDetailOrigDao;
	@Autowired
	OrderDetailOrigService orderDetailOrigService;
	
	@PostMapping
	public Response addOrder(@RequestBody OrderDetailOrig orderDetailOrig) { // json
		return orderDetailOrigService.addOrderDetailOrig(orderDetailOrig);
	}
}
