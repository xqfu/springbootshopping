package com.usc.service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.usc.dao.ProductDao;
import com.usc.http.Response;
import com.usc.beans.Product;
import com.usc.beans.User;

@Service
@Transactional
public class ProductService {
	@Autowired
	ProductDao productDao;
	
	public Response addProduct(Product product) {
		//System.out.println("product.sku="+product.getSku());
		//System.out.println("product.unit_price="+product.getUnitPrice());
		product.setActive(true);
		product.setUnitsInStock(99);
		product.setDateCreated(new Date());
		System.out.println(product);
		productDao.save(product);
		return new Response(true);
	}	
	
	public Response changeProduct(Product product) {
		Product p = productDao.findBySku(product.getSku());
		if (product.getName() !=null &&  product.getName().length() > 0)
			p.setName(product.getName());
		if (product.getDescription() !=null && product.getDescription().length() > 0)
			p.setDescription(product.getDescription());
		if (product.getUnitPrice() !=null && product.getUnitPrice().toString().length() > 0 )
			p.setUnitPrice(product.getUnitPrice());
		if (product.getImageUrl() !=null && product.getImageUrl().length() > 0)
			p.setImageUrl(product.getImageUrl());
//		product.setActive(true);
//		product.setUnitsInStock(99);
//		product.setDateCreated(new Date());
//		System.out.println(product);
		productDao.save(p);
		return new Response(true);
	}	
	public Response deleteProduct(int id) {
		if (productDao.findById(id) != null) {
			productDao.deleteById(id);
			return new Response(true);
			
		} else {
			return new Response(false, "Product is not found!");			
		}
	}

}
