package com.usc.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.usc.beans.User;
import com.usc.beans.UserProfile;
import com.usc.dao.UserDao;
import com.usc.http.Response;

@Service
@Transactional
public class UserService {
	@Autowired
	UserDao userDao;

	@Autowired
	PasswordEncoder passwordEncoder;
	
	public Response register(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		List<UserProfile> profiles = new ArrayList<UserProfile>();
		profiles.add(new UserProfile(2));
		user.setProfiles(profiles);
		System.out.println(user);
		userDao.save(user);
		return new Response(true);
	}	
	
	public Response registerAdm(User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		List<UserProfile> profiles = new ArrayList<UserProfile>();
		profiles.add(new UserProfile(1));   //admin
		user.setProfiles(profiles);
		System.out.println(user);
		userDao.save(user);
		return new Response(true);
	}
	
	public Response changeUserorPassword(User user, Authentication authentication) {
		if (isAdmin(authentication.getAuthorities())) {
			return changePassword(user, authentication);
		} else
			return changeUser(user);
	}
	
	public Response changePassword(User user, Authentication authentication) {
		if (user.getUsername().equals(authentication.getName()) || isAdmin(authentication.getAuthorities())) {
			User u = userDao.findByUsername(user.getUsername());
			u.setPassword(passwordEncoder.encode(user.getPassword()));
			userDao.save(u);
			
		} else {
			return new Response(false);
		}
		return new Response(true);	
		
	}
	
	public Response changePassword(User user) {
		User u = userDao.findByUsername(user.getUsername());
		u.setPassword(passwordEncoder.encode(user.getPassword()));
		userDao.save(u);
		
		return new Response(true);	
		
	}
	
	public Response changeUser(User user) {
		User u = userDao.findByUsername(user.getUsername());
		if (user.getPhone() !=null &&  user.getPhone().length() > 0)
			u.setPhone(user.getPhone());
		if (user.getEmail() !=null && user.getEmail().length() > 0)
			u.setEmail(user.getEmail());
		if (user.getAddress1() !=null && user.getAddress1().length() > 0 )
			u.setAddress1(user.getAddress1());
		if (user.getAddress2() !=null && user.getAddress2().length() > 0 )
			u.setAddress2(user.getAddress2());
		if (user.getCity() !=null && user.getCity().length() > 0 )
			u.setCity(user.getCity());			
		if (user.getState() !=null && user.getState().length() > 0 )
			u.setState(user.getState());				
		if (user.getZip() !=null && user.getZip().length() > 0 )
			u.setZip(user.getZip());	
		userDao.save(u);
		return new Response(true);
	}	
	
	public boolean isAdmin(Collection<? extends GrantedAuthority> profiles) {
		boolean isAdmin = false;
		for (GrantedAuthority profile : profiles) {
			if (profile.getAuthority().equals("ROLE_ADMIN")) {
				isAdmin = true;
			}
		}
		return isAdmin;
	}
	
	public Response deleteUser(int id) {
		if (userDao.findById(id) != null) {
			userDao.deleteById(id);
			return new Response(true);
			
		} else {
			return new Response(false, "User is not found!");			
		}
	}
	
	public boolean isUserAdmin(User user) {
		boolean isAdmin = false;
		Collection<? extends GrantedAuthority> profiles = user.getAuthorities();
		for (GrantedAuthority profile : profiles) {
			if (profile.getAuthority().equals("ROLE_ADMIN")) {
				isAdmin = true;
			}
		}
		return isAdmin;
	}
}
