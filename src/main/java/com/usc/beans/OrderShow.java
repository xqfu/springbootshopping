package com.usc.beans;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import org.hibernate.annotations.GenericGenerator;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "orders")  //Hibernate Entity - match the java class to the db column name
public class OrderShow {
	@Id
	@GeneratedValue(strategy= GenerationType.AUTO, generator="identity")
	@GenericGenerator(name = "identity", strategy = "native")
	private int id;
	
//    user_id           int                                  not null,
//    total_price       float     default 0                  not null,
//    date              timestamp  default CURRENT_TIMESTAMP not null on update CURRENT_TIMESTAMP,
//    shipped           tinyint(1) default 0                 null,
//    tracking_number   varchar(80)     
	
//    @Column(name = "user_id", nullable = false)
//    private int userId;

//    @OneToOne(cascade = CascadeType.PERSIST)
//    @JoinColumn(name = "product_id", referencedColumnName = "id")
//    private Product product;

	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
	@JsonIgnore
    User user;

	//  @Column(name = "orderusername")
	//  private int orderusername;

	
    @Column(name = "total_price", nullable = false)
    private BigDecimal totalPrice;

    @Column(name = "date_created")
    private Date dateCreated;

    @Column(name = "shipped")
    private boolean shipped;
    
    @Column(name = "tracking_number")
    private String trackingNumber;
    
//    @OneToMany(mappedBy="orderShow", cascade = CascadeType.ALL,fetch = FetchType.EAGER)
//    private List<OrderDetail1N> orderDetail1NList;
}
