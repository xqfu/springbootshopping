package com.usc.controller;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.usc.beans.Product;
import com.usc.dao.ProductDao;
import com.usc.dao.CartDao;
import com.usc.service.ProductService;
import com.usc.service.CartService;
import com.usc.service.UserService;

import org.springframework.test.context.ContextConfiguration;

@WebMvcTest(value = ProductController.class, excludeAutoConfiguration = { SecurityAutoConfiguration.class })
@ContextConfiguration(classes = { ProductController.class })
class ProductControllerTest {
	ObjectMapper mapper = new ObjectMapper();
	ObjectWriter writer = mapper.writer();
	
	// a mocked http server
	@Autowired 
	MockMvc mvc;
	
	@MockBean
	private ProductService productService;
	
	@MockBean
	private ProductDao productDao;
	
	@MockBean
	private ProductController productController;
	
	@MockBean
	CartDao cartitemDao;
	
	@MockBean
	CartService shopcartService;
		
	@Mock
	UserService authentication;
	
	@WithMockUser
	@Test
	void testGetAllProducts() throws Exception {
		Product product1 = new Product(1, "sku1", BigDecimal.valueOf(1));
		Product product2 = new Product(2, "sku2", BigDecimal.valueOf(2));
		Product product3 = new Product(3, "sku3", BigDecimal.valueOf(3));
		List<Product> products = new ArrayList<>();
		products.add(product1);
		products.add(product2);
		products.add(product3);
		Mockito.when(productDao.findAll()).thenReturn(products);	
		mvc.perform(MockMvcRequestBuilders
				.get("/products")				
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());				
	}
	

	@Test
	void testGetProductById_success() throws Exception {
		Product product1 =  new Product(1, "sku1", BigDecimal.valueOf(1));
		Mockito.when(productDao.findById(product1.getId())).thenReturn(Optional.of(product1));
		mvc.perform(MockMvcRequestBuilders
				.get("/products/detail").param("id", ""+product1.getId())
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
//				.andExpect(jsonPath("$", notNullValue()))
//				.andExpect(jsonPath("$.name", is("test1")))  .get("/productdetail").param("id", "1"))
				;
	}
	
	@Test
	void testAddProduct_success() throws Exception {
		Product product1 = new Product(1, "sku1", BigDecimal.valueOf(1));
		Mockito.when(productDao.save(product1)).thenReturn(product1);
		
		// generate a json from a java object
		String content = writer.writeValueAsString(product1);
		
		mvc.perform(MockMvcRequestBuilders
				.post("/products")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(content)) // requestBody
				.andExpect(status().isOk())
//				.andExpect(jsonPath("$", notNullValue()))
//				.andExpect(jsonPath("$.name", is("test1")))
				;
		
	}
//	@Disabled
	@Test
	void testDeleteProductById__success() throws Exception {
		Product product2= new Product(2, "sku2", BigDecimal.valueOf(2));
		Mockito.when(productDao.findById(2)).thenReturn(Optional.of(product2));
		
		mvc.perform(MockMvcRequestBuilders
				.delete("/products/2")
				.contentType(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk());			
		
	}


	@Test
	void testUpdatePrice() throws Exception {
		Product product1 = new Product(1, "sku1", BigDecimal.valueOf(1));
		Product updatedProduct = new Product(1, "sku1", BigDecimal.valueOf(2));
		
		Mockito.when(productDao.findById(1)).thenReturn(Optional.of(product1));
		Mockito.when(productDao.save(updatedProduct)).thenReturn(updatedProduct);
		
		String content = writer.writeValueAsString(updatedProduct);
		
		mvc.perform(MockMvcRequestBuilders
				.put("/products").param("id", "1")
				.contentType(MediaType.APPLICATION_JSON)
				.accept(MediaType.APPLICATION_JSON)
				.content(content)) // requestBody
				.andExpect(status().isOk())
//				.andExpect(jsonPath("$", notNullValue()))
//				.andExpect(jsonPath("$.unitPrice", is(2)))
				;
		
	}
	
//	//Product(int id, String sku, String name, String description, BigDecimal unitPrice, String imageUrl)
//	@WithMockUser
//	@Test
//	void testReturnProduct() throws Exception{
//		Product product = new Product(4, "sku4", "peach", "fruit", BigDecimal.valueOf(1.4), "");
//		when(productDao.findById(4)).thenReturn(Optional.of(product));
//		mvc.perform(get("/productdetail").param("id", "4")).andDo(print())
//		.andExpect(status().isOk())
//		.andExpect(jsonPath("$.id").value(4))
//		.andExpect(jsonPath("$.sku").value(product.getSku()))
//		.andExpect(jsonPath("$.name").value(product.getName()))
//		.andExpect(jsonPath("$.description").value(product.getDescription()))
//		.andExpect(jsonPath("$.unitPrice").value(product.getUnitPrice()))
//		.andExpect(jsonPath("$.imageUrl").value(product.getImageUrl()));		
//	}

//	this.mvc.perform(get("/checklogin")).andDo(print())
//	.andExpect(MockMvcResultMatchers.jsonPath("$.success").value("false"))
//	.andExpect(MockMvcResultMatchers.jsonPath("$.code").value(400))
//	.andExpect(MockMvcResultMatchers.jsonPath("$.message").value("success"));
//	  mvc.perform(MockMvcRequestBuilders
//	  			.get("/productdetail").param("id", "1"))
//	      .andDo(print())
//	      .andExpect(status().isOk())
//	      .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(1))
//	      .andExpect(MockMvcResultMatchers.jsonPath("$.sku").value("000-0001"))
//	      .andExpect(MockMvcResultMatchers.jsonPath("$.name").value("Test"));   

}
