package com.usc.dao;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.Before;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.when;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.usc.beans.Product;
import com.usc.controller.ProductController;
import com.usc.dao.ProductDao;
import com.usc.service.ProductService;

import org.springframework.test.context.ContextConfiguration;

@WebMvcTest(value = ProductController.class, excludeAutoConfiguration = { SecurityAutoConfiguration.class })
@ContextConfiguration(classes = { ProductController.class })
class ProductDaoTest {
	ObjectMapper mapper = new ObjectMapper();
	ObjectWriter writer = mapper.writer();
	
	// a mocked http server
	@Autowired 
	MockMvc mockMvc;
	
	@MockBean
	private ProductService productService;
	
	@MockBean
	private ProductDao productDao;
	
	@MockBean
	private ProductController productController;
	
//	@WithMockUser
//	@Test
//	void testGetAllProducts_success() throws Exception {
//		Product product1 = new Product(1, "sku1", BigDecimal.valueOf(1));
//		Product product2 = new Product(2, "sku2", BigDecimal.valueOf(2));
//		Product product3 = new Product(3, "sku3", BigDecimal.valueOf(3));
//		List<Product> products = new ArrayList<>();
//		products.add(product1);
//		products.add(product2);
//		products.add(product3);
//		Mockito.when(productDao.findAll()).thenReturn(products);	
//		mockMvc.perform(MockMvcRequestBuilders
//				.get("/products")				
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());				
//	}
	
//	@Test
//	void testGetProductById_success() throws Exception {
//		Product product1 = new Product(1, "sku1", BigDecimal.valueOf(1));
//		Mockito.when(productDao.findById(product1.getId())).thenReturn(Optional.of(product1));
//		
//		mockMvc.perform(MockMvcRequestBuilders
//				.get("/products/1")
//				.contentType(MediaType.APPLICATION_JSON)
//				.accept(MediaType.APPLICATION_JSON))
//				.andExpect(status().isOk());
//	}

	

}
